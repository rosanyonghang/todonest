import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TaskModule } from './task/task.module';
import { UserModule } from './user/user.module';
import { StatusModule } from './status/status.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [AppController],
  providers: [AppService],
  imports: [
    TaskModule,
    UserModule,
    StatusModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      database: 'test2',
      username: 'root',
      password: 'Vfr42wsX',
      host: 'localhost',
      port: 3306,
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
  ],
})
export class AppModule {}
